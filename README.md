# ThePrintingPress

The Printing Press project.


The project has the goal of creating a self hosted infrastructure to take back your compute and storage from big tech.
We will share plans and code here on how to do it. https://gitlab.com/jmikedupont2/theprintingpress

All modules will have code to configure and set them up. 

## Concepts
Imagine the internet goes down, or even worse it is up but malicious AI, controlled by foreign agents, is at work to spread disinformation and
prevent truth from being shared. If you only can trust a few people that you know, why not network with them?

Each tech saavy person will create an installation of `ThePrintingPress` and it will be installed at a secure location on premisis protected by the 4th 
amendment in the United states. We call this a root node. 

Other people who are in that persons network will setup `distribution nodes` as they are able. These nodes will be attached to the root node and exchange 
data as possible. CPU, Memory and storage will be shared in the collective and coin will be earned or consumed to track resource usage. 
An inventory of hardware is made and each person donating will be credited with the initial bootstrap. 

Apps will be deployed across the network, distributed and secured. Data will be stored encrypted on different devices and mirrored for redundancy and 
availability as needed.

We will have a data plane for transfer of data between nodes, so the printing press will publish prints of certain information based on tags and information into different platforms. We can hide data in plain sight, encoded into pictures and text and audio so that the AI does not recognise it immediatly.

You will own the encryption keys for your data and each data item will be able to be shared with different users. 
Publishing data to another user will involve allowing that user to decypt the data. 

Each user will get a trust score that will grow for good behavior. 
You will be able to invite and vouch for users, but that will make you responsible for them to review and approve posts and actions. If users you invite misbehave then your trust score will also go down. 

### Related ideas

* our-data https://our-data.org/
https://our-movement.org/

* Unhosted 
https://unhosted.org/

* No backend
https://nobackend.org/

* Offline First
http://offlinefirst.org/

* Mavo
https://mavo.io

#### URBIT

* URBIT 
http://urbit.org

"Urbit is a new OS and peer-to-peer network that’s simple by design, built to last forever, and 100% owned by its users. Under the hood, Urbit is a clean-slate software stack compact enough that an individual developer can understand and control it completely."
https://urbit.org/understanding-urbit/

https://degaia.co/how-to-improve-urbit-with-federated-side-chains/

#### Holochain

https://github.com/Holochain/holochain-proto/blob/whitepaper/holochain.pdf
Holochain is an open source framework for building fully distributed, peer-to-peer applications.
Holochain is BitTorrent + Git + Cryptographic Signatures + Peer Validation + Gossip.

#### Solari

* SOLARI 

Solri is a blockchain experiment that tries to bring Substrate style on-chain governance for Proof of Work.


https://solri.org/

targets WebAssembly runtime.

Based on https://github.com/paritytech/substrate, rust.


## Key Management

https://keeweb.info/ A nice password manager with a gui
https://teampass.net/ web based team password manager


## Software 

* https://nextcloud.com/ for your own cloud
* https://mastodon.social/about self hosted twitter that is federated.
* https://www.manyver.se/ this is interesting `social data live entirely in your phone`.
* https://matrix.org/ matrix chat
* https://www.mumble.info/ mumble 
* https://github.com/streamaserver/streama run your own netflix
* https://github.com/discourse/discourse another great website tool for communities

### no agenda shownotes query
https://git.sr.ht/~beshr/naq

### p2p networks
https://freenetproject.org/ freenet is the grand daddy of networks

##  RTMP RTSP WEB RTC

https://obsproject.com/forum/resources/how-to-set-up-your-own-private-rtmp-server-using-nginx.50/ RTMP is supported by nginx 

https://github.com/GRVYDEV/Project-Lightspeed A self contained OBS -> FTL -> WebRTC live streaming server

### Video editing
https://www.openshot.org/download/ Open Shot is a great video editor

### Other projects for web video streaming
* https://newpipe.net/ for downloading streams
* https://streamlink.github.io/cli.html download youtube stream
* https://github.com/deepch/RTSPtoWebRTC
* https://github.com/lulop-k/kurento-rtsp2webrtc
* https://github.com/mutaphore/RTSP-Client-Server
* https://github.com/PHZ76/RtspServer
* https://github.com/iamscottxu/obs-rtspserver OBS RTSP server


## Networking

* https://hackaday.com/2020/10/18/free-p2p-vpn/ peer to peer vpn
* https://github.com/antitree/private-tor-network running your own tor network
* https://tailscale.com/

## Source code hosting

* https://git-annex.branchable.com/tips/peer_to_peer_network_with_tor/ git annex supports tor
* https://www.cyberciti.biz/open-source/github-alternatives-open-source-seflt-hosted/ list of git alternatives.

Gitlab is quite extensive. 

## Reverse proxy

### kawapi
https://github.com/BenSchZA/kawapi-cli

### ChainFaaS
https://github.com/pacslab/ChainFaaS
https://ieeexplore.ieee.org/stamp/stamp.jsp?arnumber=9143110

### See also 
* https://hackernoon.com/public-ipfs-node-behind-nginx-reverse-proxy-5682747f174b
* https://www.corda.net/

## Forking the cryptocurrencies

In general we want to run a `test net` for etherium, bitcoin and other coins.
In an OTG senario, the global bitcoin network will not be useful. 

Private blockchains have access control:
https://www.blockchain-council.org/blockchain/public-vs-private-blockchain-a-comprehensive-comparison/

Here this guy created his own etherium chain:
https://hackernoon.com/heres-how-i-built-a-private-blockchain-network-and-you-can-too-62ca7db556c0

### Hosting at home

People say that hosting at home is a bad idea.
https://blog.ssdnodes.com/blog/data-centers-better-desktops/
https://blog.ssdnodes.com/blog/what-is-self-hosting/

First of all, why would you want to host a public site at home?
I agree that for simple html content you dont want to host that from your on prem. 

The next issue is entryway into your network. 
I maintain that you will want to do all of your compute at home and then use free hosting for public facing interface.

For example you can run cloudflare as a proxy server, have the content hosted on static hosting 
and generate the static hosting from your internal data center. 

If someone wants to run a node themselves and peer with you that is another issue, and we will discuss how that can be done. 

### Hiding in plain sight Steganography

https://towardsdatascience.com/steganography-hiding-an-image-inside-another-77ca66b2acb1 Steganography
https://github.com/kelvins/steganography
https://github.com/cedricbonhomme/Stegano
https://github.com/DominicBreuker/stego-toolkit
https://github.com/TapanSoni/BPStegano

### Games 
https://decentraland.org/
This game too  https://play.0xuniverse.com/


### Blockchain based voting

http://inno.vote/
http://votewatcher.com/
https://arxiv.org/ftp/arxiv/papers/2002/2002.07175.pdf
https://blogs.lse.ac.uk/usappblog/2020/09/25/long-read-how-blockchain-can-make-electronic-voting-more-secure/
https://core.ac.uk/download/pdf/155779036.pdf
https://dl.acm.org/doi/abs/10.1145/3019612.3019841?download=true
https://en.wikipedia.org/wiki/Helios_Voting
https://engagedscholarship.csuohio.edu/cgi/viewcontent.cgi?article=2104&context=etdarchive
https://followmyvote.com/
https://followmyvote.com/blockchain-technology/
https://github.com/IBM/evote
https://internetpolicy.mit.edu/wp-content/uploads/2020/02/SecurityAnalysisOfVoatz_Public.pdf
https://medium.com/corda/could-you-feasibly-use-a-corda-blockchain-as-a-voting-system-for-elections-89dda1cbf1ee
https://opensource.com/article/19/9/voting-fraud-open-source-solution
https://skemman.is/bitstream/1946/31161/1/Research-Paper-BBEVS.pdf
https://spectrum.ieee.org/tech-talk/computing/it/what-open-source-technology-can-cant-do-fix-elections
https://votosocial.github.io/
https://www.agora.vote/
https://www.brookings.edu/blog/techtank/2018/05/30/how-blockchain-could-improve-election-transparency/
https://www.coindesk.com/digital-voting-privacy-blockchain
https://www.coindesk.com/mit-paper-rejects-blockchain-based-voting-systems-elections
https://www.coindesk.com/snake-oil-and-overpriced-junk-why-blockchain-doesnt-fix-online-voting
https://www.finriskalert.it/wp-content/uploads/bistarelli.pdf
https://www.govtech.com/data/GT-OctoberNovember-Securing-the-Vote.html
https://www.govtech.com/products/Blockchain-Voting-Debate-Heats-Up-After-Historic-Election.html
https://www.investopedia.com/news/how-blockchain-technology-can-prevent-voter-fraud/
https://www.issuelab.org/resources/30952/30952.pdf
https://www.mdpi.com/2078-2489/11/12/552/htm
https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7554464/
https://www.researchgate.net/publication/286055741_Blockchain_Electronic_Vote


### Crypto wireless networking

https://www.helium.com/mine

###  Reusing crypto CPU sharing

We want to consider forks of CPU Coin or other tools for sharing cpu with peers.
People should be able to contribute small amounts. 

* CPU Coin https://cpucoin.io/

* Computing coins 
https://cryptoslate.com/cryptos/computing/

* https://ont.io/ Ontology
* https://iex.ec/ Decentralized Cloud Computing
iExec RLC aims at providing distributed applications running on the blockchain a scalable, secure and easy access to the services, the data-sets and the computing resources they need.
* https://www.dxchain.com/ dxchain World's first decentralized big data and machine learning network powered by a computing-centric blockchain


* https://sonm.com/ Decentralized Fog Computing Platform
* https://handbook.golem.network/  Earn tokens by renting your unused computer power
* https://akash.network/  The Unstoppable Cloud The DeCloud for DeFi, and the world's first decentralized cloud computing marketplace
* https://dos.network/ A Decentralized Oracle Service Network to Boost Blockchain Usability with Real World Data and Computation Power

#### AELF
* https://www.aelf.io/ alef 
from here https://aelf.io/gridcn/aelf_whitepaper_EN.pdf?v=1.6
2.2. Cross-Chain Interaction `aelf can interact with Bitcoin, Ethereum, and other Blockchain systems.`
2.5. Private Chain Module

```
A Considerable number of businesses are interested in Private Chains to leverage the
advantage of Blockchain technology. These private Chains usually exist in isolation
without any connection to external eco-system or other businesses. We provide a
model similar to Amazon cloud service, "AMI", where users can rapidly create an
independent Chain using our Private Chain module and obtain full ownership of it.
```

5.3.1. Smart Contract Execution
`The aelf Operating System defines Smart Contracts as Protocols. They can be executed in any forms of service realization.`
`The aelf Operating System prefers Docker, but also supports native programming languages such as Java, C#, Go, Javascript, LUA.`
`For Docker, aelf provides internal RPC services to grant access to read variables and user accounts during Smart Contract realization. For native programming languages, aelf provides respective SDKs to execution functions`
### Distributed DNS
https://dns.live/
`https://www.namebase.io/

### VPN info

https://thatoneprivacysite.net

### Distributed VPN
https://sentinel.co/

thanks to http://feedproxy.google.com/~r/survivalpcast/~3/wmIFhUM8o3w/our-decentralized-future

### Mining in the browser

https://www.forcepoint.com/blog/x-labs/browser-mining-coinhive-and-webassembly

## DAPPS

* There is a catalog of decentralized apps here https://dappstore.link/

In general we would want to fork them and setup smaller VPN network deployments of them
so instead of using existing currencies and making people buy etherium or something we would run it on our own network of servers
that is separate, so a smaller pool of mining servers that we control. 


* Metamask

The Metamask plugin can be used to connect to another testnet.
https://metamask.io/ so we can fork this as our browser interface and have it branded for each network. 


### Online Shops

* https://woocommerce.com/ online shop based on wordpress
* https://www.originprotocol.com/en/dshop

## Why 

https://www.gnu.org/philosophy/who-does-that-server-really-serve.en.html


* Fourth Amendment 
"The right of the people to be secure in their persons, houses, papers, and effects, against unreasonable searches and seizures, shall not be violated, and no warrants shall issue, but upon probable cause, supported by oath or affirmation, and particularly describing the place to be searched, and the persons or things to be seized."

* Cell Phone Addiction
https://www.nytimes.com/2019/02/23/business/cell-phone-addiction.html

## Power

* https://www.greentechmedia.com/articles/read/its-the-off-grid-data-center off the grid data center powered by gas

## Rack

### Lackrack 
* https://wiki.eth0.nl/index.php/Lackrack Lackrack

## Hardware
* Rasberry Pi 

* https://www.techrepublic.com/article/build-your-own-home-data-center-to-help-re-engage-your-technical-brain/ Build your own home datacenter
* https://beyondtech.us/blogs/beyond-blog/home-data-center Make your home a data center
* https://imaginenext.ingrammicro.com/integrated-solutions/how-i-neurotically-built-my-home-data-center How I neurotically built my home data center
* https://charlescary.com/home-data-center/ Home data center

* https://www.freenas.org/  FREENAS is now TrueNAS CORE
  * https://www.unraid.net/ UNRAID OS is not free software 



### Mobile Phones
* https://noagendaphone.com/ No Agenda Phone
* https://puri.sm/products/librem-5/ Librem 5

* Routers https://openwrt.org/
* Cell phone booster 

* http://noagendaphone.com/ 
* https://grapheneos.org/

### BBS/ Modem over cell phone

* https://ieeexplore.ieee.org/document/4383352 Data Modem for GSM 

 http://v3solar.com/wp-content/uploads/2014/05/LaDueIEEEpaper.pdf paper
 
* https://www.computer.org/csdl/pds/api/csdl/proceedings/download-article/12OmNBigFxE/pdf A Data Modem for GSM Adaptive Multi Rate Voice Channel 

* https://www.instructables.com/Computer-Time-Travel-Using-a-BBS-Over-the-Phone/ using dialup

* https://bbs.retrobattlestations.com/ example BBS

* https://my.netzero.net/start/accessNumbers.do dialup numbers

* https://hackaday.com/tag/acoustic-coupler/ build your own acoustic coupler 



### Ham Radio

https://hackaday.io/project/164092-npr-new-packet-radio new packet radio project
https://rietta.com/blog/authentication-without-encryption-for/ Authentication without encryption on ham radio
The FCC regulations say that it is forbidden to obscure the meaning of the message.
Section 97.113 (4) `messages in codes or ciphers intended to obscure the meaning thereof, except as otherwise provided herein`

https://www.n5dux.com/ham/files/pdf/Data%20Encryption%20is%20Legal.pdf Data Encryption is Legal

#### Pactor
https://www.p4dragon.com/en/PMON.html a PACTOR® Monitoring Utility for Linux
http://www.arrl.org/pactor pactor is packet radio



#### SSTV

https://ourcodeworld.com/articles/read/956/how-to-convert-decode-a-slow-scan-television-transmissions-sstv-audio-file-to-images-using-qsstv-in-ubuntu-18-04 How to decode SSTV
https://play.google.com/store/apps/details?id=xdsopl.robot36&hl=en_US&gl=US Android App Robot36 for decoding
https://github.com/davidhoness/sstv_decoder Another open source decoder
https://hackaday.com/tag/sstv/ hackaday articles
http://www.dxatlas.com/sstvtools/ SSTV tools for windows
http://www.dxatlas.com/SstvTools/Files/SstvTools.zip Download for SSTV


#### Amron 
AmRRON is a network of Preppers, Patriots and Redoubters who have volunteered to keep each other connected when other means of communications are unavailable or unreliable. 
https://amrron.com/


### Cell phone booster 

Here are some links to outback internet
https://downtoearthhomesteaders.com/6-ways-to-get-internet-access-when-living-off-grid/

https://offgridsurvival.com/offgridinternet/ 

* Lantern: A Global Satellite Data Radio
Kickstarter, 6 years and no delivery.
https://www.indiegogo.com/projects/lantern-a-global-satellite-data-radio#/comments

## Mobile 
https://lineageos.org/


### Backup

* Backup your gmail
https://github.com/krmbzds/gmvault

https://github.com/sadsfae/gbackup-rs

* How not to google
https://shahinism.com/en/posts/alternative-to-google-search/ duckduckgo and startpage


* Backup google photos 
https://github.com/nlopes/google-photos-backup


* ABD tool for backing up android
https://gist.github.com/AnatomicJC/e773dd55ae60ab0b2d6dd2351eb977c1

### Mobile mesh networking

There are some GSM mobile mesh networking tools :
* https://gotennamesh.com/ 
* https://beartooth.com/products/beartooth
* https://www.sonnetlabs.com/



# Learning

https://tlu.tarilabs.com/preface/introduction.html this is a great resource. 
https://chriswere.uk/links.html a nice list of open source apps
https://charlescarrollsociety.com/2014/01/10/american-redoubt-darknet-amrd-an-introduction/ 

